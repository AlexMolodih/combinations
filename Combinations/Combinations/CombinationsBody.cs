﻿using System.IO;
using Combinations.DTO;
using Combinations.Implementation;

namespace Combinations
{
    /// <summary>
    /// The Combinations Programme execution module.
    /// </summary>
    public class CombinationsBody
    {
        private readonly InputParser _inputParser;
        private readonly OutputPresenter _presenter;
        private readonly CombinatoricBusinessLogic _businessLogic;

        public CombinationsBody(InputParser inputParser, OutputPresenter presenter, CombinatoricBusinessLogic businessLogic)
        {
            _inputParser = inputParser;
            _presenter = presenter;
            _businessLogic = businessLogic;
        }

        /// <summary>
        /// The combinatory programm body
        /// </summary>
        /// <param name="inputFile">The input files</param>
        /// <param name="outputFile">The output file</param>
        public void Execute(string inputFile, string outputFile)
        {
            // get date from file
            InputData input;
            using (var fileStream = new FileStream(inputFile, FileMode.Open))
            {
                input = _inputParser.GetData(fileStream);
            }
            
            // process entered data
            var results = _businessLogic.GetCombinations(input);

            // publish data to file
            using (var fileStream = new FileStream(outputFile, FileMode.Create))
            {
                _presenter.PresentData(results, fileStream);
            }
        }
    }
}