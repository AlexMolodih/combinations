﻿using System;

namespace Combinations.Exceptions
{
    public class DataFormatException : Exception
    {
        public DataFormatException(string message)
            : base(message)
        {
        }
    }
}