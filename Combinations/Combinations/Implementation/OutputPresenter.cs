﻿using System.IO;
using System.Linq;
using System.Text;
using Combinations.DTO;

namespace Combinations.Implementation
{
    /// <summary>
    /// Outputs combinations to file
    /// </summary>
    public class OutputPresenter
    {
        /// <summary>
        /// Presents data in readable format and writes to stream
        /// </summary>
        /// <param name="results">The input data for formatting</param>
        /// <param name="stream">The output stream</param>
        public void PresentData(ProcessingResults results, Stream stream)
        {
            var lines = results.WordCombinations.Select(it => string.Join(" ", it)).ToList();

            using (StreamWriter outputFile = new StreamWriter(stream, Encoding.UTF8, 100, true))
            {
                foreach (string line in lines)
                    outputFile.WriteLine(line);
            }
        }
    }
}
