﻿using System.Collections.Generic;
using System.Linq;
using Combinations.DTO;

namespace Combinations.Implementation
{
    /// <summary>
    /// Represents implementation for business logic which creates combinations.
    /// It the history you can check initial version of algorithm I came up with.
    /// </summary>
    public class CombinatoricBusinessLogic
    {
        /// <summary>
        /// Get combinations for defined set of words
        /// </summary>
        public ProcessingResults GetCombinations(InputData input)
        {
            var uniqueInput = input.Words.Distinct().ToArray();

            var result = CombinationsRetrieving(uniqueInput, input.N, new HashSet<string>()).ToList();
            
            return new ProcessingResults
            {
                WordCombinations = result
            };
        }

        private IEnumerable<ISet<string>> CombinationsRetrieving(string[] allWords, uint n, HashSet<string> combinations, int i = 0, bool isSecondLeg = false)
        {
            // Current cobination is ready, print it
            if ((combinations.Count <= n) && (combinations.Count > 0) && !isSecondLeg)
            {
                yield return new HashSet<string>(combinations);
            }

            // Got to maximal limit no more serch required
            if (combinations.Count == n)
            {
                yield break;
            }

            // Got the final word no more elements are there 
            if (i >= allWords.Length)
            {
                yield break;
            }
            
            // current is included, put next at next location
            combinations.Add(allWords[i]);
            foreach (var item in CombinationsRetrieving(allWords, n, combinations, i + 1))
            {
                yield return item;
            } 
            
            // current is excluded, replace it with next one
            combinations.Remove(allWords[i]);
            foreach (var item in CombinationsRetrieving(allWords, n, combinations, i + 1, true))
            {
                yield return item;
            }
        }
    }
}
