﻿using System;
using System.IO;
using Combinations.Exceptions;

namespace Combinations.Implementation
{
    /// <summary>
    /// The input parser that reads data from sources
    /// </summary>
    public class InputParser
    {
        /// <summary>
        /// Reads data from stream and converts it to object
        /// </summary>
        /// <param name="source">The stream which reprents data source</param>
        /// <returns>The parsed data</returns>
        public DTO.InputData GetData(Stream source)
        {
            var result = new DTO.InputData();

            var text = ReadFromStream(source);
            var rows = GetRows(text);
            result.N = GetN(rows[0]);
            result.Words = rows[1].Split(' ');

            return result;
        }

        private static string[] GetRows(string text)
        {
            var result = text.Split(new[] {"\r\n"}, StringSplitOptions.None);
            if (result.Length < 2)
            {
                throw new DataFormatException("File should have two lines");
            }

            return result;
        }

        private static uint GetN(string text)
        {
            uint n;

            if (!uint.TryParse(text, out n))
            {
                throw new DataFormatException("The first line cannot be converted to number");
            }
            return n;
        }

        private string ReadFromStream(Stream stream)
        {
            using (StreamReader sr = new StreamReader(stream))
            {
                return sr.ReadToEnd();
            }
        }
    }
}
