﻿using System;
using System.IO;
using Combinations.Exceptions;
using Combinations.Implementation;

namespace Combinations
{
    class Program
    {
        static void Main(string[] args)
        {
            // small validation
            if (Validate(args))
            {
                Console.ReadKey();
                return;
            }

            // description about programm
            Console.WriteLine("Program reads data from file [{0}] and writes word combinations to [{1}]", 
                args[0],
                args[1]);

            // Programm execution
            try
            {
                var programm = new CombinationsBody(new InputParser(), new OutputPresenter(),
                    new CombinatoricBusinessLogic());
                programm.Execute(args[0], args[1]);
            }
            catch (DataFormatException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("File not fount");
            }
            catch (Exception)
            {
                Console.WriteLine("Unexpected error ocured");
            }

            // wait for user input
            Console.WriteLine("Press any key to continue");

            Console.ReadKey();
        }

        private static bool Validate(string[] args)
        {
            if ((args.Length < 1) || string.IsNullOrEmpty(args[0]))
            {
                Console.WriteLine("Input file is not specified");
                return true;
            }

            if ((args.Length < 2) || string.IsNullOrEmpty(args[1]))
            {
                Console.WriteLine("Output file is not specified");
                return true;
            }
            return false;
        }
    }
}
