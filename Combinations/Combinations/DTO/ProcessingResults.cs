﻿using System.Collections.Generic;

namespace Combinations.DTO
{
    /// <summary>
    /// The results of data processing as a list of combinations
    /// </summary>
    public class ProcessingResults
    {
        /// <summary>
        /// The Words Combinations where the set represent represents each combination
        /// </summary>
        public IList<ISet<string>> WordCombinations { get; set; }
    }
}