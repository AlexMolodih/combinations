﻿using System.Collections.Generic;

namespace Combinations.DTO
{
    /// <summary>
    /// The input data from user.
    /// </summary>
    public class InputData
    {
        /// <summary>
        /// The N which represents a max number of words in combimations.
        /// </summary>
        public uint N { get; set; }

        /// <summary>
        /// The words whis are doing to be used in combinations.
        /// </summary>
        public string[] Words { get; set; }
    }
}