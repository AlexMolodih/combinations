﻿using System.Collections.Generic;
using System.IO;
using Combinations.DTO;
using Combinations.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class FileOutputTest
    {
        [TestMethod]
        public void DataPresenterTest()
        {
            // Set up
            var inputData = new ProcessingResults
            {
                WordCombinations = new List<ISet<string>>
                { 
                    new HashSet<string>(){"A","B"},
                    new HashSet<string>(){"C","D"}
                }
            };
            var outputStream = new MemoryStream();
            var presenter = new OutputPresenter();

            // Execute
            presenter.PresentData(inputData, outputStream);

            // Asserts
            var streamReader = new StreamReader(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            var result = streamReader.ReadToEnd();
            Assert.IsTrue(result.StartsWith("A B\r\nC D"));
        }
    }
}
