﻿using System.Collections.Generic;
using System.Linq;
using Combinations.DTO;
using Combinations.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class CombinatoricBusinessLogicTest
    {
        [TestMethod]
        public void CaseWhenNLessThanWordsAmount()
        {
            // Set up
            var businessLogic = new CombinatoricBusinessLogic();
            var inputData = new InputData
            {
                N = 3, 
                Words = new[] {"A", "B", "C", "D"}
            };
            var expectedResult = new List<ISet<string>>
            {
                new HashSet<string> {"A"},
                new HashSet<string> {"B"},
                new HashSet<string> {"C"},
                new HashSet<string> {"D"},
                new HashSet<string> {"A", "B"},
                new HashSet<string> {"B", "C"},
                new HashSet<string> {"C", "D"},
                new HashSet<string> {"A", "C"},
                new HashSet<string> {"B", "D"},
                new HashSet<string> {"A", "D"},
                new HashSet<string> {"B", "C", "D"},
                new HashSet<string> {"A", "B", "C"},
                new HashSet<string> {"A", "B", "D"},
                new HashSet<string> {"A", "C", "D"}
            };

            // Execute
            var results = businessLogic.GetCombinations(inputData);

            // Assert
            Assert.AreEqual(expectedResult.Count, results.WordCombinations.Count);
            AssertArraysHaveTheSameCombinations(expectedResult, results.WordCombinations);
        }
        
        [TestMethod]
        public void CaseWhenNBiggerThanWordsAmount()
        {
            // Set up
            var fileInput = new CombinatoricBusinessLogic();
            var inputData = new InputData
            {
                N = 7,
                Words = new[] { "A", "B", "C", "D" }
            };
            var expectedResult = new List<ISet<string>>
            {
                new HashSet<string> {"A"},
                new HashSet<string> {"B"},
                new HashSet<string> {"C"},
                new HashSet<string> {"D"},
                new HashSet<string> {"A", "B"},
                new HashSet<string> {"B", "C"},
                new HashSet<string> {"C", "D"},
                new HashSet<string> {"A", "C"},
                new HashSet<string> {"B", "D"},
                new HashSet<string> {"A", "D"},
                new HashSet<string> {"B", "C", "D"},
                new HashSet<string> {"A", "B", "C"},
                new HashSet<string> {"A", "B", "D"},
                new HashSet<string> {"A", "C", "D"},
                new HashSet<string> {"A", "B", "C", "D"}
            };

            // Execute
            var results = fileInput.GetCombinations(inputData);

            // Assert
            Assert.AreEqual(expectedResult.Count, results.WordCombinations.Count);
            AssertArraysHaveTheSameCombinations(expectedResult, results.WordCombinations);
        }

        [TestMethod]
        public void CaseWhenDublicates()
        {
            // Set up
            var fileInput = new CombinatoricBusinessLogic();
            var inputData = new InputData
            {
                N = 7,
                Words = new[] { "A", "B", "C", "D", "A" }
            };
            var expectedResult = new List<ISet<string>>
            {
                new HashSet<string> {"A"},
                new HashSet<string> {"B"},
                new HashSet<string> {"C"},
                new HashSet<string> {"D"},
                new HashSet<string> {"A", "B"},
                new HashSet<string> {"B", "C"},
                new HashSet<string> {"C", "D"},
                new HashSet<string> {"A", "C"},
                new HashSet<string> {"B", "D"},
                new HashSet<string> {"A", "D"},
                new HashSet<string> {"B", "C", "D"},
                new HashSet<string> {"A", "B", "C"},
                new HashSet<string> {"A", "B", "D"},
                new HashSet<string> {"A", "C", "D"},
                new HashSet<string> {"A", "B", "C", "D"}
            };

            // Execute
            var results = fileInput.GetCombinations(inputData);

            // Assert
            Assert.AreEqual(expectedResult.Count, results.WordCombinations.Count);
            AssertArraysHaveTheSameCombinations(expectedResult, results.WordCombinations);
        }

        private static void AssertArraysHaveTheSameCombinations(IList<ISet<string>> expectedResult, IList<ISet<string>> results)
        {
            foreach (var expectedCombination in expectedResult)
            {
                Assert.IsNotNull(results.SingleOrDefault(it => expectedCombination.SequenceEqual(it)),
                    string.Format("Combination [{0}] is not represented in result", string.Join(" ", expectedCombination)));
            }
        }
    }
}
