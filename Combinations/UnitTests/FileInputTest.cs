﻿using System.Collections.Generic;
using System.IO;
using Combinations.Exceptions;
using Combinations.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class FileInputTest
    {
        [TestMethod]
        public void PositiveCase()
        {
            // Set up
            var inputData = CreateStreamFromString("3\r\nA B C D");
            var parser = new InputParser();
            
            // Execute
            var results = parser.GetData(inputData);

            // Asserts
            Assert.AreEqual(3U, results.N);
            CollectionAssert.AreEqual(new List<string> {"A", "B", "C", "D"}, results.Words);
        }

        [TestMethod]
        [ExpectedException(typeof(DataFormatException))]
        public void OneRowReturned()
        {
            // Set up
            var inputData = CreateStreamFromString("3");
            var parser = new InputParser();

            // Execute
            parser.GetData(inputData);
        }
        
        [TestMethod]
        [ExpectedException(typeof(DataFormatException))]
        public void FirstRowNotNumber()
        {
            // Set up
            var inputData = CreateStreamFromString("A\r\nA B C D");
            var parser = new InputParser();

            // Execute
            parser.GetData(inputData);
        }

        private static MemoryStream CreateStreamFromString(string fileData)
        {
            var repo = new MemoryStream();

            var stringBytes = System.Text.Encoding.UTF8.GetBytes(fileData);
            repo.Write(stringBytes, 0, stringBytes.Length);
            repo.Seek(0, SeekOrigin.Begin);
            return repo;
        }
    }
}
